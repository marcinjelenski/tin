#!/usr/bin/python

# http://stackoverflow.com/questions/1934675/how-to-execute-python-scripts-in-windows
# https://docs.python.org/2/faq/windows.html#id3

import os
import interpreter
import argparse

# TODO: wykrywanie czy libki/programy nie maja takiej samej nazwy

# Parse the input arguments.
argument_parser = argparse.ArgumentParser()
argument_parser.add_argument("-i", default=None,
                             dest='input_script',
                             help='input script filepath',
                             required=False)
argument_parser.add_argument("module", nargs="*",
                             help='specific module to be built')
args = argument_parser.parse_args()

# Evaluate the input script path.
if args.input_script is None:
    input_script_path = os.path.join(os.getcwd(), "build_config")
else:
    input_script_path = os.path.abspath(args.input_script)

# Check if the file exists.
if not os.path.isfile(input_script_path):
    # TODO: exception
    print 'input file "' + input_script_path + '" does not exist!'
    exit(-1)

# Change working directory.
os.chdir(os.path.dirname(input_script_path))

# Open the file.
with open(input_script_path) as f:
    script = f.read()

# Parse the file.
interpreter.parse(script)

