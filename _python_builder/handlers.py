from objects.for_ import For
from objects.assignment import Assignment
from objects.statementlist import StatementList
from objects.if_ import If
from objects.function import Function
from objects.array import Array
from objects.argumentlist import ArgumentList
from objects.expression import Expression
from objects.condition import Condition
from objects.stringconcatenation import StringConcatenation
from objects.variable import Variable
from objects.string import String
from utils.operators import operator_map

__author__ = 'Bartek'

from utils.errors import ParserTypeError


def handle_str_concatenation(p):
    # Make a list of all contained elements for further validation.
    objects = []
    # Expected types of arguments.
    simple_types = [String, Variable]
    complex_types = [StringConcatenation]
    # Handle the left and the right operand, respectively.
    for operand in [p[1], p[3]]:
        if operand.__class__ in simple_types:
            objects.append(operand)
        elif operand.__class__ in complex_types:
            objects.extend(operand.element_list)
        else:
            raise ParserTypeError(simple_types + complex_types, operand.type)
    # Combine the value and the type of new object.
    p[0] = StringConcatenation(objects)


def handle_str_lib(p):
    # Create a new object of type str_lib.
    p[0] = String(p[1][1:-1], String.STR_LIB)


def handle_str_path(p):
    # Create a new object of type str_path.
    p[0] = String(p[1][1:-1], String.STR_PATH)


def handle_var(p):
    # Create a new object of type var.
    p[0] = Variable(p[1])


def handle_condition(p):
    # Create a new object (condition) containing operands and operators.
    p[0] = Condition(p[1], p[3], operator_map[p[2]])


def handle_expression_complex(p):
    # Create a new object (expression) containing operands and operators.
    p[0] = Expression(p[2], p[4], operator_map[p[3]])


def handle_expression_condition(p):
    # Pass along the condition.
    p[0] = p[2]


def handle_expression_negated(p):
    # Check if expression or condition.
    p[0] = p[2].negated()


def handle_array(p):
    # Create an array object.
    p[0] = Array(p[2])


def handle_operator(p):
    # Pass along the operator string.
    p[0] = p[1]


def handle_multiple_arg_list(p):
    # Add the argument to the existing list.
    arg_list = p[1]
    arg_list.append(p[3])
    p[0] = arg_list


def handle_single_arg_list(p):
    # Make a list containing the argument.
    try:
        val = [p[1]]
    except IndexError:
        val = []
    # Create a new object.
    p[0] = ArgumentList(val)


def handle_function(p):
    # Create an object with (value, arg_list) as a value.
    p[0] = Function(p[1], p[3])


def pass_production(p):
    # Pass the production along.
    p[0] = p[1]


def handle_block(p):
    # Pass the statement list along.
    p[0] = p[2]


def handle_elif_element(p):
    # Create a list containing a tuple (condition, block).
    p[0] = [If.IfClause(p[2], p[3])]


def handle_elif_list(p):
    # Add another elif statement to the existing list.
    elif_list = p[1]
    elif_list.append(If.IfClause(p[3], p[4]))
    # Create a new object.
    p[0] = elif_list


def handle_if(p):
    # Create a list of ifs/elifs/else.
    if_clause = If.IfClause(p[2], p[3])
    elif_clauses_list = None
    else_clause = None
    try:
        elif_clauses_list = p[4]
        else_clause = If.IfClause(None, p[6])
    except IndexError:
        pass
    p[0] = If(if_clause, elif_clauses_list, else_clause)


def handle_if_else(p):
    if_clause = If.IfClause(p[2], p[3])
    else_clause = If.IfClause(None, p[5])
    p[0] = If(if_clause, None, else_clause)


def handle_statement_list(p):
    # Add the argument to the existing list.
    statement_list = p[1]
    statement_list.append(p[2])
    # Create a new object.
    p[0] = statement_list


def handle_statement_list_element(p):
    # Make a list containing the statement.
    if len(p) > 2:
        val = [p[1]]
    else:
        val = []
    # Create a new object.
    p[0] = StatementList(val)


def handle_assignment(p):
    # Create an assignment statement.
    p[0] = Assignment(p[1], p[3])


def handle_for(p):
    # Create a for object.
    p[0] = For(p[2], p[4], p[5])
