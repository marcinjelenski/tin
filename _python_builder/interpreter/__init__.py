__author__ = 'Bartek'

import ply.lex as lex
import ply.yacc as yacc
import lexer
import parser
import parsetab

generated_lexer = lex.lex(module=lexer)
generated_parser = yacc.yacc(module=parser)

def parse(s):
    generated_parser.parse(s, lexer=generated_lexer)