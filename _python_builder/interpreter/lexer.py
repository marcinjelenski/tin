import ply.lex as lex

__author__ = 'Bartek'

reserved = {"if": "IF", "for": "FOR", "in": "IN", "else": "ELSE", "and": "AND",
            "or": "OR", "not": "NOT", "elif": "ELIF",
            "lib": "FUN_LIB", "setGlobalIncludeDir": "FUN_SETGID",
            "setGlobalLibraryDir": "FUN_SETGLD", "program": "FUN_PROGRAM",
            "setFlags": "FUN_SETFLAGS", "setCompiler": "FUN_SETCOMPILER",
            "setOutputDir": "FUN_SETOUTPUTDIR"}

tokens = [
             "VAR_NAME", "STR_LIB", "STR_PATH", "EQUAL", "LESS_E", "GREATER_E",
             "NOT_EQUAL",
         ] + list(reserved.values())

literals = ['(', ')', '=', ',', '[', ']', '+', '{', '}', ';', '>', '<']

# Token definitions.

def t_VAR_NAME(t):
    r'[a-zA-Z_][a-zA-Z0-9_]*'
    t.type = reserved.get(t.value, 'VAR_NAME')  # Check for reserved words
    return t


t_STR_LIB = r'"[a-zA-Z0-9_]*"'
# t_str_path_STR = r'"(([A-Z|a-z]:\\[^*|"<>?\n]*)|(\\\\.*?\\.*))"'
t_STR_PATH = r'"([^|"<>\n]*)"'

t_EQUAL = "=="
t_NOT_EQUAL = "!="
t_LESS_E = "<="
t_GREATER_E = ">="

t_ignore = " \t"


def t_newline(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")


def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

def get_tokens():
    return tokens + literals