import handlers
from interpreter import lexer
from utils.functions import build

tokens = lexer.get_tokens()

precedence = (
    ('left', ','),
    ('left', '+')
)

start = "script"


def p_script(p):
    """
    script : statement_list
    """
    # main.main_function(p[1])
    p[1].evaluate()
    build()


def p_statement_list(p):
    """
    statement_list : statement_list statement ';'
    """
    handlers.handle_statement_list(p)


def p_statement_list_element(p):
    """
    statement_list : statement ';'
                   | ';'
    """
    handlers.handle_statement_list_element(p)


def p_statement(p):
    """
    statement : assignment
              | for
              | if
              | function
    """
    handlers.pass_production(p)


def p_assignment(p):
    """
    assignment : VAR_NAME '=' arg
    """
    handlers.handle_assignment(p)


def p_for(p):
    """
    for : FOR VAR_NAME IN var block
        | FOR VAR_NAME IN array block
    """
    handlers.handle_for(p)


def p_if(p):
    """
    if : IF expression block
       | IF expression block elif_list
       | IF expression block elif_list ELSE block
    """
    handlers.handle_if(p)


def p_if_else(p):
    """
    if : IF expression block ELSE block
    """
    handlers.handle_if_else(p)


def p_elif_list(p):
    """
    elif_list : elif_list ELIF expression block
    """
    handlers.handle_elif_list(p)


def p_elif_element(p):
    """
    elif_list : ELIF expression block
    """
    handlers.handle_elif_element(p)


def p_block(p):
    """
    block : '{' statement_list '}'
    """
    handlers.handle_block(p)


def p_arg(p):
    """
    arg : var
        | function
        | str
        | array
    """
    handlers.pass_production(p)


def p_var(p):
    """
    var : VAR_NAME
    """
    handlers.handle_var(p)


def p_array(p):
    """
    array : '[' arg_list ']'
    """
    handlers.handle_array(p)


def p_function(p):
    """
    function : fun_name '(' arg_list ')'
    """
    handlers.handle_function(p)


def p_multiple_arg_list(p):
    """
    arg_list : arg_list "," arg
    """
    handlers.handle_multiple_arg_list(p)


def p_single_arg_list(p):
    """
    arg_list : arg
             |
    """
    handlers.handle_single_arg_list(p)


def p_str_concatenation(p):
    """
    str : str '+' str
        | str '+' var
        | var '+' str
        | var '+' var
    """
    handlers.handle_str_concatenation(p)


def p_str_lib(p):
    """
    str : STR_LIB
    """
    handlers.handle_str_lib(p)


def p_str_path(p):
    """
    str : STR_PATH
    """
    handlers.handle_str_path(p)


def p_fun_name(p):
    """
    fun_name : FUN_LIB
             | FUN_PROGRAM
             | FUN_SETGLD
             | FUN_SETGID
             | FUN_SETFLAGS
             | FUN_SETCOMPILER
             | FUN_SETOUTPUTDIR
    """
    handlers.pass_production(p)


def p_expression_complex(p):
    """
    expression : '(' expression logic_op expression ')'
    """
    handlers.handle_expression_complex(p)


def p_expression_condition(p):
    """
    expression : '(' condition ')'
    """
    handlers.handle_expression_condition(p)


def p_expression_negated_condition(p):
    """
    expression : NOT expression
    """
    handlers.handle_expression_negated(p)


def p_condition(p):
    """
    condition : var condition_op var
              | var condition_op str
              | str condition_op var
              | str condition_op str
    """
    handlers.handle_condition(p)


def p_condition_op(p):
    """
    condition_op : '<'
                 | '>'
                 | EQUAL
                 | NOT_EQUAL
                 | LESS_E
                 | GREATER_E
    """
    handlers.handle_operator(p)


def p_logic_op(p):
    """
    logic_op : AND
             | OR
    """
    handlers.handle_operator(p)


def p_error(p):
    if p:
        print("Syntax error at '%s'" % p.value)
    else:
        print("Syntax error at EOF")