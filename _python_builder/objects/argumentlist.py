from objects.obj import Obj

__author__ = 'Bartek'


class ArgumentList(Obj):
    def __init__(self, element_list):
        Obj.__init__(self)
        self.element_list = element_list

    def evaluate(self):
        evaluated_list = []
        for element in self.element_list:
            evaluated_list.append(element.evaluate())
        return ArgumentList(evaluated_list)

    def append(self, element):
        self.element_list.append(element)

    def get_iterable(self):
        return self.element_list