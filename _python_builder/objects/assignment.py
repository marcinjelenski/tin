from objects.obj import Obj
from objects.variable import Variable

__author__ = 'Bartek'


class Assignment(Obj):
    def __init__(self, name, value):
        Obj.__init__(self)
        self.name = name
        self.value = value

    def evaluate(self):
        Variable(self.name, self.value.evaluate())