from objects.obj import Obj

__author__ = 'Bartek'


class BuildArtifactID(Obj):

    def __init__(self, id):
        Obj.__init__(self)
        self.id = id

    def get_id(self):
        return self.id

    def get_iterable(self):
        return [self]