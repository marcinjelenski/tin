from objects.obj import Obj
from utils.operators import negated_operator

__author__ = 'Bartek'


class Condition(Obj):
    def __init__(self, left_op, right_op, operator):
        Obj.__init__(self)
        self.left_operand = left_op
        self.right_operand = right_op
        self.operator = operator

    def negate(self):
        self.operator = negated_operator(self.operator)

    def evaluate(self):
        left_op = self.left_operand.evaluate().get_value()
        right_op = self.right_operand.evaluate().get_value()
        return self.operator(left_op, right_op)