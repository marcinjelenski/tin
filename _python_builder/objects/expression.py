from objects.obj import Obj

__author__ = 'Bartek'


class Expression(Obj):
    def __init__(self, left_op, right_op, operator):
        Obj.__init__(self)
        self.left_operand = left_op
        self.right_operand = right_op
        self.operator = operator
        self.negated = False

    def negate(self):
        self.negated = not self.negated

    def evaluate(self):
        left_op = self.left_operand.evaluate()
        right_op = self.right_operand.evaluate()
        if self.negated:
            return not self.operator(left_op, right_op)
        else:
            return self.operator(left_op, right_op)