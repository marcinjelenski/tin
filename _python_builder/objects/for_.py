from objects.obj import Obj
from objects.variable import Variable

__author__ = 'Bartek'


class For(Obj):
    def __init__(self, var, iterable, block):
        Obj.__init__(self)
        self.var = var
        self.iterable = iterable
        self.block = block

    def evaluate(self):
        for el in self.iterable.evaluate().get_iterable():
            Variable(self.var, el)
            self.block.evaluate()