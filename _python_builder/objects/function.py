from objects.obj import Obj
from utils import functions

__author__ = 'Bartek'


class Function(Obj):
    def __init__(self, name, argument_list):
        Obj.__init__(self)
        self.name = name
        self.argument_list = argument_list

    def evaluate(self):
        arg_list = self.argument_list.evaluate().get_iterable()
        return functions.call(self.name, arg_list)