from objects.obj import Obj

__author__ = 'Bartek'


class If(Obj):

    class IfClause:
        def __init__(self, expression, block):
            self.expression = expression
            self.block = block

    def __init__(self, if_clause, elif_clauses_list, else_clause):
        Obj.__init__(self)
        self.if_clause = if_clause
        if elif_clauses_list is None:
            self.elif_clauses_list = []
        else:
            self.elif_clauses_list = elif_clauses_list
        self.else_clause = else_clause

    def evaluate(self):
        if self.if_clause.expression.evaluate():
            self.if_clause.block.evaluate()
            return
        for elif_clause in self.elif_clauses_list:
            if elif_clause.expression.evaluate():
                elif_clause.block.evaluate()
                return
        if self.else_clause is not None:
            self.else_clause.block.evaluate()