__author__ = 'Bartek'


class NameDict:
    def __init__(self):
        self.dict = {}

    def set(self, key, value):
        # if key in self.dict.keys():
        # # print "Overriding a value!"
        # # return
        # pass
        self.dict[key] = value

    def get(self, key):
        if key not in self.dict.keys():
            # TODO: exception
            print "Variable " + key + " not defined!"
            return
        return self.dict[key]

    def clear(self):
        self.dict = {}