from objects.obj import Obj
from utils.errors import ParserTypeError

__author__ = 'Bartek'


class String(Obj):
    # TODO: doc
    """
    Doc stub.
    """

    STR_LIB = 1
    STR_PATH = 2

    def __init__(self, value, string_type):
        """Create new string of given type and with given value.

        :param value: string value, e.g. "str"
        :param string_type: string type. Supported types: String.STR_LIB,
            String.STR_PATH.
        :return: new object
        """
        Obj.__init__(self)
        self.value = value
        self.string_type = string_type
        if string_type not in [self.STR_LIB, self.STR_PATH]:
            raise ParserTypeError(["String.STR_LIB", "String.STR_PATH"],
                                  string_type)

    def evaluate(self):
        """Return self - a string cannot be evaluated further.

        :return: self
        """
        return self

    def get_value(self):
        """Return the value of string object.

        :return: value
        """
        return self.value

    def get_type(self):
        """Return the type of the string.

        :return: String.STR_LIB or String.STR_PATH
        """
        return self.string_type

    def get_iterable(self):
        return [self]