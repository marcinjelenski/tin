from objects.obj import Obj
from objects.string import String
from objects.variable import Variable
from utils.errors import ParserTypeError

__author__ = 'Bartek'


class StringConcatenation(Obj):
    def __init__(self, element_list):
        """Create a concatenation object.

            :param element_list: list of elements to evaluate and concatenate.
            :return: new object
            """
        Obj.__init__(self)
        self.element_list = []
        for el in element_list:
            if el.__class__ not in [String, StringConcatenation, Variable]:
                raise ParserTypeError([String, StringConcatenation, Variable],
                                      el)
            self.element_list.append(el)

    def evaluate(self):
        """Return an evaluated string.

        :return: evaluated string
        """
        types = []
        value = ""
        for element in self.element_list:
            types.append(element.evaluate().get_type())
            value += element.evaluate().get_value()
        types = set(types)
        str_type = String.STR_PATH if String.STR_PATH in types \
            else String.STR_LIB
        return String(value, str_type)