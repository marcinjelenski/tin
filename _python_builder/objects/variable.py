from objects.namedict import NameDict
from objects.obj import Obj

__author__ = 'Bartek'


class Variable(Obj):
    names = NameDict()

    def __init__(self, name, value=None):
        """Create a new variable identifier object.

        :param name: name of the variable.
        :return: new variable identifier object
        """
        Obj.__init__(self)
        self.name = name
        if value is not None:
            self.names.set(self.name, value)

    def evaluate(self):
        return self.names.get(self.name).evaluate()

    def get_object(self):
        return self.names.get(self.name)

    @staticmethod
    def clear_context():
        Variable.names.clear()

    @staticmethod
    def get_context():
        return Variable.names