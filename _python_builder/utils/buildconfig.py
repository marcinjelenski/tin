import os
import subprocess

# TODO: nazwa lib musi byc przycieta! str.strip()
# TODO: budowanie tylko czesci programu
# TODO: sprawdzic ponizsze:
# Przy budowaniu static library nic sie nie linkuje - wiec dependencies
# licza sie tylko przy finalnym budowaniu. Sprawdzic?
import errno
from utils.defaults import DEFAULT_OUTPUT_DIR


class BuildConfig():
    artifact_id = 0

    class ArtifactDictPosition:
        def __init__(self, art_type=None, sources=None, dependencies=None,
                     users=None, name=None):
            if sources is not None:
                self.sources = sources
            else:
                self.sources = []
            try:
                self.dependencies = [lib.get_id() for lib in dependencies]
            except TypeError:
                self.dependencies = []
            try:
                self.users = [lib.get_id() for lib in users]
            except TypeError:
                self.users = []
            self.name = name
            self.type = art_type

    def __init__(self):
        self.compiler = "g++"
        self.global_include_dirs = []
        self.global_library_dirs = []
        # TODO: implement setter
        self.flags = []
        # TODO: implement setter, create output dir
        self.output_dir = DEFAULT_OUTPUT_DIR
        self.artifacts_dict = {}

    def get_dependencies_for_artifact(self, artifact_id):
        # Find positions that have defined the artifact as their user.
        indirect_dependencies = []
        for art in self.artifacts_dict:
            if artifact_id in self.artifacts_dict[art].users:
                indirect_dependencies.append(art)
        direct_dependencies = self.artifacts_dict[artifact_id].dependencies
        dependencies = []
        for dep in direct_dependencies + indirect_dependencies:
            dependencies.append(dep)
            dependencies.extend(
                self.get_dependencies_for_artifact(dep))
        return dependencies

    def set_global_include_dirs(self, include_dirs):
        self.global_include_dirs = include_dirs

    def set_global_library_dirs(self, library_dirs):
        self.global_library_dirs = library_dirs

    def set_flags(self, flags_split):
        self.flags = flags_split

    def set_compiler(self, compiler):
        if compiler in ["gcc", "g++"]:
            self.compiler = compiler

    def set_output_dir(self, output_dir):
        self.output_dir = output_dir

    def add_artifact(self, art_type=None, sources=None, dependencies=None,
                     users=None, name=None):
        artifact_id = BuildConfig.artifact_id
        BuildConfig.artifact_id += 1
        self.artifacts_dict[artifact_id] = BuildConfig.ArtifactDictPosition(
            art_type, sources, dependencies, users, name)
        return artifact_id

    def build(self):
        # print "global_library_dirs:", self.global_library_dirs
        # print "global_include_dirs:", self.global_include_dirs
        sorted_artifacts = self._sort_artifacts()
        # print "sorted artifacts:", sorted_artifacts
        for artifact_id in reversed(sorted_artifacts):
            art = self.artifacts_dict[artifact_id]
            if art.type == 'lib':
                # print "---"
                # print "artifact name:", art.name.get_value()
                # print "artifact sources:", art.sources
                # print "artifact dependencies:", art.dependencies
                # print "artifact users:", art.users
                self._build_lib(artifact_id)
        for artifact_id in sorted_artifacts:
            art = self.artifacts_dict[artifact_id]
            if art.type == "program":
                # print "---"
                # print "artifact name:", art.name.get_value()
                # print "artifact sources:", art.sources
                # print "artifact dependencies:", art.dependencies
                self._build_program(artifact_id, sorted_artifacts)

    def _build_lib(self, artifact_id):
        artifact = self.artifacts_dict[artifact_id]
        if len(artifact.sources) == 0:
            return
        name = artifact.name.get_value()
        object_files = []
        # Build object files.
        for source in artifact.sources:
            command = [self.compiler]
            command.extend(self.flags + ["-c"])
            command.append(source)
            # Append output sources.
            output_file = os.path.join(self.output_dir, name, os.path.splitext(
                os.path.basename(source))[0] + ".o")
            self._make_sure_path_exists(os.path.dirname(output_file))
            object_files.append(output_file)
            command.extend(["-o",  output_file])
            # Append include directories.
            for include_dir in self.global_include_dirs:
                command.extend(["-I", include_dir])
            # TODO: log
            print " ".join(command)
            subprocess.call(command)
        # Create the static library.
        output_file = os.path.join(self.output_dir, "lib" + name + ".a")
        command = ["ar", "rcs", output_file] + object_files
        # TODO: log
        print " ".join(command)
        self._make_sure_path_exists(os.path.dirname(output_file))
        subprocess.call(command)

    def _build_program(self, artifact_id, sorted_artifacts):
        artifact = self.artifacts_dict[artifact_id]
        # Append the compiler with flags.
        args = [self.compiler]
        args.extend(self.flags)
        # Append sources.
        for source in artifact.sources:
            args.append(source)
        # Append output sources.
        output_file = os.path.join(self.output_dir, artifact.name.get_value())
        args.extend(["-o", output_file])
        # Append include directories.
        for include_dir in self.global_include_dirs:
            args.extend(["-I", include_dir])
        # Append library directories.
        for library_dir in self.global_library_dirs:
            args.extend(["-L", library_dir])
        # Figure out the dependencies.
        libs = []
        dependencies = self.get_dependencies_for_artifact(artifact_id)
        for art_id in sorted_artifacts:
            if art_id in dependencies:
                libs.append("-l" + self.artifacts_dict[art_id].name.get_value())
        if len(libs) > 0:
            args.append("-L"+self.output_dir)
            args.extend(libs)
        # TODO: log
        print " ".join(args)
        self._make_sure_path_exists(os.path.dirname(output_file))
        subprocess.call(args)

    def _sort_artifacts(self):
        # Mapping from artifact to another dictionary containing elements
        # that the artifact is dependent of (key: 'before'), and elements
        # that are dependent of that artifact (key: 'after').
        dependency_dict = {}
        # Iterate over all artifacts.
        for artifact in self.artifacts_dict:
            # Create keys in the dictionary.
            if artifact not in dependency_dict.keys():
                dependency_dict[artifact] = {'before': [], 'after': []}
            # Handle the artifacts that are dependent of the artifact.
            for dependency in self.artifacts_dict[artifact].dependencies:
                # Create keys in the dictionary.
                if dependency not in dependency_dict.keys():
                    dependency_dict[dependency] = {'before': [], 'after': []}
                # The artifact needs to be linked before the dependency.
                dependency_dict[artifact]['before'].append(dependency)
                # The dependency needs to be linked after the artifact.
                dependency_dict[dependency]['after'].append(artifact)
            # Handle the artifacts that the artifact is dependent of.
            for user in self.artifacts_dict[artifact].users:
                # Create keys in the dictionary.
                if user not in dependency_dict.keys():
                    dependency_dict[user] = {'before': [], 'after': []}
                # The artifact needs to be linked after its user.
                dependency_dict[artifact]['after'].append(user)
                # The user needs to be linked before used artifact.
                dependency_dict[user]['before'].append(artifact)
        # Create dict mapping an artifact to its relative position
        # in the linking queue - the higher the value the further
        # in the queue the artifact is.
        value_dict = dict((art, None) for art in dependency_dict)
        # Iterate over the dictionary created earlier.
        for artifact in dependency_dict:
            # Elements that go before the artifact in the linking queue.
            elements_before = dependency_dict[artifact]['after']
            # Elements that go after the artifact in the linking queue.
            elements_after = dependency_dict[artifact]['before']
            # Same lists but with values evaluated from value_dict.
            elements_before = [value_dict[art] for art in elements_before
                               if value_dict[art] is not None]
            elements_after = [value_dict[art] for art in elements_after
                              if value_dict[art] is not None]
            # Max position of elements that need to be linked before.
            left_lim = None if len(elements_before) == 0 \
                else max(elements_before)
            # Min position of elements that need to be linked after.
            right_lim = None if len(elements_after) == 0 \
                else min(elements_after)
            # Find the artifact's position in the queue.
            if left_lim is None and right_lim is None:
                value_dict[artifact] = 0.0
            elif left_lim is None and right_lim is not None:
                value_dict[artifact] = right_lim - 1.0
            elif left_lim is not None and right_lim is None:
                value_dict[artifact] = left_lim + 1.0
            elif left_lim is not None and right_lim is not None:
                if left_lim == right_lim:
                    # TODO: exception
                    exit(31337)
                value_dict[artifact] = (left_lim + right_lim) / 2.0
        # Return the list of the artifacts sorted by their values.
        return sorted(value_dict, key=value_dict.get)

    @staticmethod
    def _make_sure_path_exists(path):
        try:
            os.makedirs(path)
        except OSError as exception:
            if exception.errno != errno.EEXIST:
                raise

