__author__ = 'Bartek'


class ParserTypeError(TypeError):

    def __init__(self, expected, got):
        error_string = "expected one of the following types: " + str(expected)
        error_string += ", got " + repr(got)
        super(ParserTypeError, self).__init__(error_string)