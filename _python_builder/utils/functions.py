import glob

from objects.buildartifacts import BuildArtifactID
from objects.string import String
from utils.buildconfig import BuildConfig


build_config = BuildConfig()

__author__ = 'Bartek'


def build():
    build_config.build()


# FIXME: glob musi znajdowac w katalogu, w ktorym znajduje sie skrypt konfiguracyjny!
# FIXME ciag dalszy: http://stackoverflow.com/questions/3430372/
def lib(args):
    lib_name = args[0]
    try:
        lib_sources = args[1]
        sources = []
        for source in lib_sources.get_iterable():
            sources.extend(glob.glob(source.get_value()))
    except IndexError:
        sources = None
    try:
        lib_dependencies = args[2]
        dependencies = []
        for dep in lib_dependencies.get_iterable():
            dependencies.append(dep)
    except IndexError:
        dependencies = None
    try:
        lib_users = args[3]
        users = []
        for user in lib_users.get_iterable():
            users.append(user)
    except IndexError:
        users = None
    # Check if name is of proper type.
    if lib_name.get_type() != String.STR_LIB:
        # TODO: exception
        return
    # Evaluate the paths.
    # TODO: stala
    id = build_config.add_artifact(art_type="lib", name=lib_name, sources=sources,
                                   dependencies=dependencies, users=users)
    return BuildArtifactID(id)


def program(args):
    program_name = args[0]
    program_sources = args[1]
    sources = []
    for source in program_sources.get_iterable():
        sources.extend(glob.glob(source.get_value()))
    try:
        program_dependencies = args[2]
        dependencies = []
        for dep in program_dependencies.get_iterable():
            dependencies.append(dep)
    except IndexError:
        dependencies=None
    # Check if name is of proper type.
    if program_name.get_type() != String.STR_LIB:
        # TODO: exception
        return
    # Evaluate the paths.
    # TODO: stala
    id = build_config.add_artifact(art_type="program", name=program_name,
                                   sources=sources, dependencies=dependencies)
    return BuildArtifactID(id)


def setGlobalIncludeDir(include_dirs):
    include_dirs = include_dirs[0]
    paths = []
    for include_dir in include_dirs.get_iterable():
        paths.extend(glob.glob(include_dir.get_value()))
    build_config.set_global_include_dirs(paths)


def setGlobalLibraryDir(library_dirs):
    library_dirs = library_dirs[0]
    paths = []
    for library_dir in library_dirs.get_iterable():
        paths.extend(glob.glob(library_dir.get_value()))
    build_config.set_global_library_dirs(paths)


def setFlags(args):
    flags_split = []
    flags = args[0].get_iterable()
    for flag in flags:
        flags_split.extend(flag.get_value().split(" "))
    build_config.set_flags(flags_split)


def setCompiler(args):
    build_config.set_compiler(args[0].get_value())


def setOutputDir(args):
    build_config.set_output_dir(args[0].get_value())


def call(function_name, args):
    if function_name == "lib":
        return lib(args)
    if function_name == "program":
        return program(args)
    if function_name == "setGlobalIncludeDir":
        return setGlobalIncludeDir(args)
    if function_name == "setGlobalLibraryDir":
        return setGlobalLibraryDir(args)
    if function_name == "setFlags":
        return setFlags(args)
    if function_name == "setCompiler":
        return setCompiler(args)
    if function_name == "setOutputDir":
        return setOutputDir(args)