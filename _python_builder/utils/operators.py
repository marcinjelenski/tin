import operator

__author__ = 'Bartek'


operator_map = {
    '==': operator.eq,
    '!=': operator.ne,
    '<=': operator.le,
    '>=': operator.ge,
    '<':  operator.lt,
    '>':  operator.gt,
    'and': operator.and_,
    'or': operator.or_
}

def negated_operator(condition_operator):
    # TODO: docs
    """Negate the condition's operator and return a negated condition.

    :param condition: Object of type Obj. The field `type` must be equal to
        commons.TYPE_CONDITION.
    :return: negated condition.
    """
    negated = {
        operator.eq: operator.ne,
        operator.ne: operator.eq,
        operator.le: operator.gt,
        operator.ge: operator.lt,
        operator.lt: operator.ge,
        operator.gt: operator.le
    }
    # Return a negated condition.
    return negated[condition_operator]

