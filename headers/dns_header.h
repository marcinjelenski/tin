# ifndef H_DNS_HEADER
# define H_DNS_HEADER

#include <netinet/in.h>

// Typy rekordów DNS
#define TYPE_A 1
#define TYPE_NS 2
#define TYPE_CNAME 5
#define TYPE_SOA 6
#define TYPE_PTR 12
#define TYPE_MX 15

// Struktura nagłówka dns
typedef struct DNS_HEADER
{
	unsigned short id; 					// identyfikator

	unsigned char rd :1; 				// kopiowane z zapytania
	unsigned char tc :1; 				// 1 jeśli wiadomość nie zmieściła się w 1 odpowiedzi
	unsigned char aa :1; 				// określa czy odpowiedź jest autoratywna
	unsigned char opcode :4; 		// przepisujemy do odpowiedzi
	unsigned char qr :1; 				// 0 - pytanie, 1 - odpowiedź

	unsigned char rcode :4; 		// response code
	unsigned char cd :1; 				// checking disabled
	unsigned char ad :1; 				// authenticated data
	unsigned char z :1; 				// musi być 0
	unsigned char ra :1; 				// określa czy serwer umożliwia zapytania rekurencyjne

	unsigned short q_count; 		// liczba zapytań
	unsigned short answer_count;// number of answer entries
	unsigned short auth_count;	// number of authority entries
	unsigned short add_count; 	// number of resource entries
} DNS_HEADER;

// PYTANIE
typedef struct QUESTION
{
	unsigned short qtype;
	unsigned short qclass;			// IPv4 czy IPv6
} QUESTION;

//Constant sized fields of the resource record structure
#pragma pack(push, 1)
typedef struct R_DATA
{
	unsigned short type;
	unsigned short _class;
	unsigned int ttl;
	unsigned short data_len;
} R_DATA;
#pragma pack(pop)

//Pointers to resource record contents
typedef struct RES_RECORD
{
	unsigned char *name;
	struct R_DATA *resource;
	unsigned char *rdata;
} RES_RECORD;

// Struktura zapytania
typedef struct
{
	unsigned char *name;
	QUESTION *ques;
} QUERY;


// zapytanie wstawiane do kolejki
typedef struct
{
	// struct sockaddr_in cliaddr; // pytający
	int name_len; // dlugosc nazwy hosta
} QUEUED_QUERY;

# endif