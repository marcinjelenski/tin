# ifndef H_DNS_UTILS
# define H_DNS_UTILS

using namespace std;

#include <stdio.h>
#include <unistd.h>
#include <string>
#include <cstring>
#include <fstream>
#include <vector>
#include <sstream>
#include "dns_header.h"

// stałe
#define MAX_LEN 255

// konwertujemy nazwę z 6google2pl na .google.pl
int dnsToDot(unsigned char* name,unsigned char* result);

// konwertuje .google.pl na 6google2pl
int dotToDns(unsigned char* name,unsigned char* result);

void printAsInt(unsigned char *tab);

int min(int a, int b);

bool file_exists(const string& filename);

// pobranie nagłówka z przekazanej wiadomości
DNS_HEADER* getHeaderFromMessage(unsigned char *mesg);

// pobranie nazwy hosta z przekazanej wiadomości
unsigned char* getNameFromMessage(unsigned char *mesg);

// pobranie zapytania z przekazanej wiadomości
QUESTION* getQuestionFromMessage(unsigned char *mesg);

vector<string> explode(const string& str, const char delimiter);

#endif