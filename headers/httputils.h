#ifndef H_HTTP
#define H_HTTP

#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;

#define HTTP_RESP_OK "HTTP/1.1 200 OK"
#define HTTP_ACCEPT_RANGE_BYTES "Accept-Ranges: bytes"
#define HTTP_CONTENT_TYPE_HTML "Content-Type: text/html"
#define HTTP_CONTENT_TYPE_CSS "Content-Type: text/css"
#define HTTP_CONTENT_TYPE_JS "Content-Type: text/javascript"
#define HTTP_CONTENT_LENGTH "Content-Length: "
#define HTTP_RN "\r\n"
#define HTTP_DOUBLE_RN "\r\n\r\n"

#define METHOD_EMPTY	0
#define METHOD_GET		1
#define METHOD_POST		2

string getRequestPath(string &request);
string getResponseLength(int length);
string wrapOkResponse(string &contentType, string &response);
string wrapRightDiv(string &content);

#endif