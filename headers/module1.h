# ifndef H_MODULE1_HEADER
# define H_MODULE1_HEADER

#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <pthread.h>
#include <string>
#include "msghandler.h"
#include "xmlhandler.h"	
#include "dns_header.h"	

#define REPLY_IF_RULE_NOT_EXISTS 0 // 1 lub 0!
#define IP_LEN 4
#define DEFAULT_TTL 71
#define NAME_COMPR_SIZE 2
#define NAME_COMPR {0xC0, 0x0C}

using namespace std;

class Module1
{
private:
	pthread_t threadId;			// id wątku
	MsgHandler* msgHandler; 	// instancja do obsługi kolejek
	XmlHandler* xmlHandler;		// instancja do obsługi konfiguracji w pliku xml
	mqd_t requestQueue; 		// kolejka do zostawiania zapytań

	static void* startDNS(void *arg);									// start
	bool onQueryReceived(int sockfd, struct sockaddr_in &cliaddr, unsigned char* mesg, int N); // obsługa zapytania
	void reply(Rule &rule, unsigned char* mesg, int sockfd, struct sockaddr_in &cliaddr); // 
public:
	bool running;

	Module1(MsgHandler* msgHandler, XmlHandler* xmlHandler);
	int startModule();		// uruchomienie modułu w wątku
	void stopModule();		// zatrzymanie wątku modułu
};

# endif