# ifndef H_MODULE2_HEADER
# define H_MODULE2_HEADER

#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <pthread.h>
#include <string>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <vector>
#include <unistd.h>
#include <arpa/inet.h>
#include "msghandler.h"	
#include "xmlhandler.h"		
#include "dns_header.h"

#define GUI_PORT 6066

#define bufferSize 1024			// Rozmiar bufora
#define CONNECTIONS_LIMIT 10		// Limit otwartych połączeń
#define JSON_BATCH_LIMIT 30		// Limit otwartych połączeń

#define ROUTE_QUEUE "queue" // adres do kolejki

#define ROUTE_CONFIG "config" // adres do zapisu konfiguracji
#define ROUTE_CONFIG_SIZE 6 // rozmiar adresu konfiguracji

using namespace std;

class Module2
{
private:
	pthread_t threadId;			// id wątku
	MsgHandler* msgHandler; 	// instancja do obsługi kolejek
	XmlHandler* xmlHandler;		// instancja do obsługi konfiguracji w pliku xml
	mqd_t requestQueue; 		// kolejka do zostawiania zapytań

	string onRequestCome(string &contentType, string &path); // akcja wykonywana po przyjściu requestu; path: ścieżka
	string getQueueElements(); // pobieranie elementów z kolejki jako obiekt JSON
	string saveToConfig(string &path); // zapisuje do konfiguracji

	// pozostałe funkcje
	string getFileContents(string fileName); 	// zwraca zawartość pliku
	static void* startServer(void *arg);		// start
public:
	bool running;

	Module2(MsgHandler* msgHandler, XmlHandler* xmlHandler);
	int startModule();		// uruchomienie modułu w wątku
	void stopModule();		// zatrzymanie wątku modułu
};

# endif