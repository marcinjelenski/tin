# ifndef H_MSG_HANDLER
# define H_MSG_HANDLER

#include <fcntl.h>           /* For O_* constants */
#include <sys/stat.h>        /* For mode constants */
#include <mqueue.h>
#include <stdio.h>
#include <errno.h>

#define REQUEST_QUEUE_NAME "/request_queue"
#define RESPONSE_QUEUE_NAME "/response_queue"
#define MAX_QUEUE_SIZE 10240

class MsgHandler
{
public:
	// Deskryptor kolejki wejsciowej.
	mqd_t msg_queue_request;
	// Deskryptor kolejki wyjsciowej.
	mqd_t msg_queue_response;

	MsgHandler() {};

	// Tworzy kolejki. Zwraca 0 jesli sukces, -1 jesli jedna z kolejek
	// nie zostala poprawnie utworzona.
	int initQueues();

	// Zamyka kolejki. Zwraca 0 jesli sukces, -1 jesli jedna z kolejek
	// nie zostala poprawnie zamknieta.
	int closeQueues();

	// Zwraca kolejke request w danym trybie ('r' lub 'w'). Zwraca -1
	// jesli nie dalo sie otworzyc kolejki, -2 jesli niepoprawny tryb.
	mqd_t getRequestQueue(char mode);

	// Zwraca kolejke response w danym trybie ('r' lub 'w'). Zwraca -1
	// jesli nie dalo sie otworzyc kolejki, -2 jesli niepoprawny tryb.
	mqd_t getResponseQueue(char mode);
};

# endif