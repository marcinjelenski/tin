#ifndef H_RULE
#define H_RULE

#include <string>
using namespace std;

enum RuleType {
	EMPTY,
	BLOCK,
	ALLOW,
	REDIRECT
};

class Rule
{
private:

	RuleType rule_type;
	string name;
	string redirect_ip;

public:

	Rule();

	void setRuleType(RuleType rule_type);
	void setName(string name);
	void setRedirectIP(string redirect_ip);
	RuleType getRuleType();
	string getName();
	string getRedirectIP();
};

#endif