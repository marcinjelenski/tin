# ifndef H_XML_HANDLER
# define H_XML_HANDLER

#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_print.hpp"
#include "rapidxml/rapidxml_utils.hpp"
#include "rule.h"
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
//#include <cstring>

#define MAX_PATH_LEN 1024
#define CFG_NODE_NAME "config"
#define BL_NODE_NAME "black_list"
#define WL_NODE_NAME "white_list"
#define REDIR_NODE_NAME "redirect_list"
#define ITEM_NODE_NAME "item"
#define DOMAIN_NODE_NAME "domain"
#define REDIRECTIP_NODE_NAME "new_ip"
#define XML_DECLARATION "<?xml version=\"1.0\" encoding=\"utf-8\"?>"

using namespace rapidxml;
using namespace std;

class XmlHandler
{
private:

	string filepath;
	xml_document<>* doc;
	xml_node<>* black_list;
	xml_node<>* white_list;
	xml_node<>* redirect_list;

	int save();	// save wykonuje sie automatycznie po kazdej operacji!

public:


	XmlHandler();
	~XmlHandler();
	void createEmptyConfig(string path);
	int open(string path);
	int addWhiteListRule(string host_name);
	int addBlackListRule(string host_name);
	int addRedirectListRule(string host_name, string new_ip_string);
	bool getRule(string host_name, Rule* rule);
	int deleteRule(string host_name);
	int getRuleCount();
	int getAllRules(Rule* rule_set, int n_rules);
};



# endif