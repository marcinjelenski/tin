CC=g++ -std=c++0x
HEADERS=headers
SOURCES=sources
CFLAGS=-I $(HEADERS) -c -std=gnu++0x -pthread
LFLAGS=-pthread
LFLAGSEND=-lrt
OUTPUTDIR=build
OBJS=$(OUTPUTDIR)/main.o $(OUTPUTDIR)/httputils.o $(OUTPUTDIR)/dnsutils.o $(OUTPUTDIR)/module1.o $(OUTPUTDIR)/module2.o $(OUTPUTDIR)/msghandler.o $(OUTPUTDIR)/xmlhandler.o $(OUTPUTDIR)/rule.o
OUTPUTNAME=dns


dns: $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o $(OUTPUTDIR)/$(OUTPUTNAME) $(LFLAGSEND)

$(OUTPUTDIR)/main.o: $(SOURCES)/main.cpp
	$(CC) $(CFLAGS) $(SOURCES)/main.cpp -o $(OUTPUTDIR)/main.o

$(OUTPUTDIR)/httputils.o: $(SOURCES)/httputils.cpp
	$(CC) $(CFLAGS) $(SOURCES)/httputils.cpp -o $(OUTPUTDIR)/httputils.o

$(OUTPUTDIR)/dnsutils.o: $(SOURCES)/dnsutils.cpp
	$(CC) $(CFLAGS) $(SOURCES)/dnsutils.cpp -o $(OUTPUTDIR)/dnsutils.o

$(OUTPUTDIR)/module1.o: $(SOURCES)/module1.cpp
	$(CC) $(CFLAGS) $(SOURCES)/module1.cpp -o $(OUTPUTDIR)/module1.o

$(OUTPUTDIR)/module2.o: $(SOURCES)/module2.cpp
	$(CC) $(CFLAGS) $(SOURCES)/module2.cpp -o $(OUTPUTDIR)/module2.o

$(OUTPUTDIR)/msghandler.o: $(SOURCES)/msghandler.cpp
	$(CC) $(CFLAGS) $(SOURCES)/msghandler.cpp -o $(OUTPUTDIR)/msghandler.o

$(OUTPUTDIR)/xmlhandler.o: $(SOURCES)/xmlhandler.cpp
	$(CC) $(CFLAGS) $(SOURCES)/xmlhandler.cpp -o $(OUTPUTDIR)/xmlhandler.o

$(OUTPUTDIR)/rule.o: $(SOURCES)/rule.cpp
	$(CC) $(CFLAGS) $(SOURCES)/rule.cpp -o $(OUTPUTDIR)/rule.o

cleanw:
	del /S $(OBJS) $(OUTPUTNAME).exe

cleanl:
	rm -f /S $(OBJS) $(OUTPUTDIR)/$(OUTPUTNAME)
