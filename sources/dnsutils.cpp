#include "dnsutils.h"
#include <stdio.h>
#include <iostream>
#include <netinet/in.h>

int dnsToDot(unsigned char* name,unsigned char* result)
{
	int len = strlen((const char*)name);
	if(result == nullptr)
	{
		return len+1;
	}
	int dest = 0;
	for (int i = 0; i < len; i++)
	{
		int offset = name[i]; // 6
		result[i] = '.';
		dest = i + offset;
		for(int j = i + 1; j <= dest; j++)
		{
			result[j] = name[j];
		}
		i = dest;
	}
	result[len] = 0;
	return len;
}

// konwertuje .google.pl na 6google2pl
// podajemy nullptr w zmiennej result aby dowiedzieć się
// o wymaganym rozmiarze result
int dotToDns(unsigned char* name, unsigned char* result)
{
	int len = strlen((const char*)name) + 2;

	if(len > MAX_LEN)
	{
		len = MAX_LEN;
	}

	if(result == nullptr)
	{
		return len;
	}

	result[len-1] = 0;
	int i = len-2;
	unsigned char cnt = 0;
	while(i>0)
	{
		if(name[i-1] == '.')
		{
			result[i] = cnt;
			cnt = 0; // zerujemy licznik
		} else {
			cnt++;
			result[i] = name[i-1];
		}
		i--;
	}
	result[0] = cnt;

	return len;
}

int min(int a, int b)
{
	return (a > b ? b : a);
}

bool file_exists(const std::string& filename) {
  ifstream ifile(filename.c_str());
  return ifile;
}

DNS_HEADER* getHeaderFromMessage(unsigned char* mesg)
{
	return (DNS_HEADER*) mesg;
}

unsigned char* getNameFromMessage(unsigned char* mesg)
{
	int offset = sizeof(DNS_HEADER);
	return (unsigned char*) &mesg[offset];
}

QUESTION* getQuestionFromMessage(unsigned char* mesg)
{
	unsigned char* name = getNameFromMessage(mesg);

	int offset = sizeof(DNS_HEADER) + (strlen((const char*)name)+1);
	return (QUESTION*) (mesg + offset);
}

// rozbijanie stringa na wektor
vector<string> explode(const string& str, const char delimiter)
{
  vector<string> elements;
  stringstream stream(str);
  string item;
  while (getline(stream, item, delimiter))
    elements.push_back(item);
 
  return elements;
}