#include "httputils.h"
#include <string>
#include <sstream>
#include <regex>

using namespace std;

string getRequestPath(string &query)
{
	if(query.length() < 7)
		return "";

	int method = METHOD_EMPTY;
	string rest;
	if(query.substr(0,4) == "GET ")
	{
		method = METHOD_GET;
		rest = query.substr(4);
	} else if(query.substr(0,5) == "POST ")
	{
		method = METHOD_POST;
		rest = query.substr(5);
	}

	if(method == METHOD_EMPTY)
		return "";

	int endpos = min(rest.find(" HTTP"), rest.length());

	return rest.substr(1, endpos-1);
}

string getResponseLength(int length)
{
	return HTTP_CONTENT_LENGTH + to_string(length);
}

string wrapOkResponse(string &contentType, string &response)
{
	std::stringstream ss;
	
	ss << HTTP_RESP_OK << HTTP_RN;
	ss << HTTP_ACCEPT_RANGE_BYTES << HTTP_RN;
	ss << contentType << HTTP_RN;
	ss << getResponseLength(response.length()) << HTTP_DOUBLE_RN;
	ss << response;

	return ss.str();
}

string wrapRightDiv(string &content)
{
	return "<div style=\"text-align:right\">"+content+"</div>";
}