#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <pthread.h>

#include "msghandler.h"
#include "xmlhandler.h"
#include "dnsutils.h"

#include "module1.h"
#include "module2.h"
#include "rule.h"

using namespace std;

// Funkcja uruchomieniowa
int main()
{
	// XML --------------------------------------------------------------------

	XmlHandler* xh = new XmlHandler();

	// Utworz nowy plik konfiguracyjny jeśli nie istnieje.
	if(!file_exists("cfg/config.xml"))
	{
		xh->createEmptyConfig("cfg/config.xml");
	}

	// Otworz plik konfiguracyjny.
	xh->open((char*)"cfg/config.xml");

	//  PROBA MSGHANDLER
	MsgHandler mh = MsgHandler();
	mh.initQueues();
	// PROBA MSGHANDLER
	
	// Start modułu 1
	Module1* m1 = new Module1(&mh, xh);
	int err = m1->startModule();
	if (err != 0)
	  cout<<"\nBlad tworzenia watku DNS: "<<err<<"\n";
	else
	  cout<<"\nWatek dns utworzony pomyślnie.\n";

	// Start modułu 2
	Module2* m2 = new Module2(&mh, xh);
	int err2 = m2->startModule();
	if (err2 != 0)
	  cout<<"\nBlad tworzenia watku TCP: "<<err2<<"\n";
	else
	  cout<<"\nWatek TCP utworzony pomyślnie.\n";

	// Komenda "stop" zatrzymuje program
	string input = "";
	cout << endl << "\033[1;31mKomenda \"stop\" zatrzymuje program.\033[0m\n" << endl;
	
	while(true) {
		getline(cin, input);

		if (input == "stop") {
			m1->stopModule();	// Zakończenie wątku modułu 1
			m2->stopModule();	// Zakończenie wątku modułu 2
			mh.closeQueues();	// Zakończenie kolejek

			cout << endl << "\033[1;31mProgram zakończył pracę.\033[0m\n" << endl;
			break;			
		}
		else {
			cout << endl << "\033[1;31mProgram oczekuje tylko na komendę \"stop\".\033[0m" << endl;
		}
	}

	// Czekamy na zakończenie wątków
	pthread_exit(NULL);

	delete xh;
	delete m2;
	delete m1;
}
