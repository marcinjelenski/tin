#include "module1.h"	
#include "dns_header.h"
#include "dnsutils.h"
#include "dnsutils.h"
#include "rule.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>

#define MAX_MSG_LEN 4096

Module1::Module1(MsgHandler* msgHandler, XmlHandler* xmlHandler)
{
	this->msgHandler = msgHandler;
	this->xmlHandler = xmlHandler;
	this->running = true;
}

int Module1::startModule()
{
	this->requestQueue = this->msgHandler->getRequestQueue('w');

	int err = pthread_create(&(this->threadId), NULL, &Module1::startDNS, this);
	return err;
}

void Module1::stopModule()
{
	this->running = false;

	int err = pthread_cancel(this->threadId);
	if (err != 0) {
		perror("Błąd zakończenia wątku modułu 1.");
	}
	pthread_join(this->threadId, NULL);
}

void* Module1::startDNS(void *arg)
{
	Module1* instance = (Module1*) arg;

	int sockfd; // deskryptor socketu
	int n;	// liczba odczytanych bajtów

	struct sockaddr_in servaddr, cliaddr;
	socklen_t len;
	unsigned char msg[MAX_MSG_LEN];

	sockfd = socket(AF_INET,SOCK_DGRAM,0);

	bzero(&servaddr,sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(53);
	int rbind = bind(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr));

	if (rbind != 0) {
    perror("DNS cannot bind to 53 port");
  } else {
		cout<<"DNS module started" << endl;
		while(instance->running)
		{
			len = sizeof(cliaddr);
			n = recvfrom(sockfd,msg,MAX_MSG_LEN,0,(struct sockaddr *)&cliaddr,&len);
			instance->onQueryReceived(sockfd, cliaddr, msg, n);
		}
	}

	return 0;
}

void Module1::reply(Rule &rule, unsigned char* mesg, int sockfd, struct sockaddr_in &cliaddr)
{
	DNS_HEADER* srcDnsHead = getHeaderFromMessage(mesg);
	DNS_HEADER* dnsHead = new DNS_HEADER(*srcDnsHead);

	unsigned char* name = getNameFromMessage(mesg);
	QUESTION* question = getQuestionFromMessage(mesg);

	bool redirect = false;
	switch (rule.getRuleType())
	{
		case RuleType::EMPTY:
		case RuleType::BLOCK:
			dnsHead->rcode = 3;
			dnsHead->answer_count = htons(0);
			cout<<"Zablokuj:    "<<rule.getName();
			break;
		case RuleType::REDIRECT:
			dnsHead->rcode = 0;
			dnsHead->answer_count = htons(1);
			redirect = true;
			cout<<"Przekieruj:  "<<rule.getName()<<" -> "<<rule.getRedirectIP();
			break;
		case RuleType::ALLOW:
			dnsHead->rcode = 5;
			dnsHead->answer_count = htons(0);
			cout<<"Zezwól:      "<<rule.getName();
			break;
		default:
			cout<<"Fatal error: unknown rule type.\n";
			exit(1);
	}
	cout<<endl;

	// Przygotowujemy header do odpowiedzi.
	dnsHead->qr = 1;
	dnsHead->aa = 0;
	dnsHead->tc = 0;
	dnsHead->ra = 1;
	dnsHead->q_count = htons(1);
	dnsHead->auth_count = htons(0);
	dnsHead->add_count = htons(0);

	// Rozmiar calej wiadomosci.
	int sizeName = strlen((const char*)name) + 1;
	int sizeAllResp = sizeof(DNS_HEADER) + sizeName + sizeof(QUESTION);

	// Tworzymy strukture R_DATA (rfc1035 strona 28).
	unsigned char* redirectIP;
	R_DATA rdata = R_DATA();
	// Przygotowujemy informacje o adresie (jesli przekierowujemy).
	if (redirect)
	{
		rdata.type = question->qtype;
		rdata._class = question->qclass;
		rdata.ttl = htons(DEFAULT_TTL) << 16;
		rdata.data_len = (IP_LEN) << 8;	
		redirectIP = new unsigned char[IP_LEN];
		inet_aton(rule.getRedirectIP().c_str(), (struct in_addr*) redirectIP);		
		sizeAllResp += NAME_COMPR_SIZE + sizeof(R_DATA) + IP_LEN;
	}
	
	// Alokujemy pamiec na odpowiedz.
	unsigned char* respData = new unsigned char[sizeAllResp];

	// Przesuniecie w strukturze odpowiedzi.
	int offset = 0;

	// header
	memcpy(respData + offset, dnsHead, sizeof(DNS_HEADER));
	offset += sizeof(DNS_HEADER);

	// name
	memcpy(respData + offset, name, sizeName);
	offset += sizeName;

	// question
	memcpy(respData + offset, question, sizeof(QUESTION));
	offset += sizeof(QUESTION);	

	if (redirect)
	{
		// name - skompresowany
		unsigned char namePointer[NAME_COMPR_SIZE] = NAME_COMPR;
		memcpy(respData + offset, namePointer, NAME_COMPR_SIZE);
		offset += NAME_COMPR_SIZE;	

		// rdata
		memcpy(respData + offset, &rdata, sizeof(R_DATA));
		offset += sizeof(R_DATA);

		// redirectIP
		memcpy(respData + offset, redirectIP, IP_LEN);
		offset+=IP_LEN;		
	}

	sendto(sockfd, respData, offset, 0, (struct sockaddr *) &cliaddr, sizeof(cliaddr));

	if (redirect)
	{
		delete redirectIP;
	}
	delete dnsHead;
	delete respData;
}

bool Module1::onQueryReceived(int sockfd, struct sockaddr_in &cliaddr, unsigned char* mesg, int N)
{
	mesg[N] = 0; // zabezpieczenie
	// header
	DNS_HEADER 	*dnsHead = getHeaderFromMessage(mesg);
	// nazwa
	unsigned char *name = getNameFromMessage(mesg);
	int nameSize = dnsToDot(name, nullptr);
	unsigned char *name_readable = new unsigned char[nameSize];
	dnsToDot(name, name_readable);
	// zapytanie
	QUESTION *question = getQuestionFromMessage(mesg);

	int questionCount = ntohs(dnsHead->q_count);

	Rule rule = Rule();

	bool rule_exists = this->xmlHandler->getRule((char*)name_readable, &rule);

	if(rule_exists)
	{
		reply(rule, mesg, sockfd, cliaddr);
	} else	 
	{
		// nie znamy odpowiedzi, przesyłamy do kolejki
		string name = string((const char*) name_readable);
		QUEUED_QUERY qq = QUEUED_QUERY();
		qq.name_len = name.length();
		int mesg_size = sizeof(QUEUED_QUERY) + name.length() + 1;
		unsigned char* queue_msg = new unsigned char[mesg_size];
		memcpy(queue_msg, &qq, sizeof(QUEUED_QUERY));
		memcpy(queue_msg + sizeof(QUEUED_QUERY), name.c_str(), name.length());
		queue_msg[mesg_size-1] = '\0';

		mq_send(this->requestQueue, (const char*) queue_msg, mesg_size, 0);

		Rule temporaryRule = Rule();
		temporaryRule.setRuleType(RuleType::EMPTY);
		temporaryRule.setName(name);

		if (REPLY_IF_RULE_NOT_EXISTS)
		{
			// odpowiedz blokujaca
			reply(temporaryRule, mesg, sockfd, cliaddr);
		}
	}

	delete name_readable;
	return true; // znamy odpowiedź, więc odpowiadamy
}
