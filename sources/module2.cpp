#include "module2.h"	
#include "dns_header.h"
#include "dnsutils.h"
#include "httputils.h"
#include "rule.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <string>
#include <sstream>
#include <vector>
#include <iterator>

#define MAX_MSG_LEN 65000

Module2::Module2(MsgHandler* msgHandler, XmlHandler* xmlHandler)
{
	this->msgHandler = msgHandler;
	this->xmlHandler = xmlHandler;
	this->running = true;
}

int Module2::startModule()
{
	this->requestQueue = this->msgHandler->getRequestQueue('r');

	int err = pthread_create(&(this->threadId), NULL, &Module2::startServer, this);
	return err;
}

void Module2::stopModule()
{
	this->running = false;

	int err = pthread_cancel(this->threadId);
	if (err != 0) {
		perror("Błąd zakończenia wątku modułu 2.");
	}
	pthread_join(this->threadId, NULL);
}

void* Module2::startServer(void *arg)
{
	Module2* instance = (Module2*) arg;

	int socketDescriptor,
	socketDescriptorConn,
	responseValue;
	
	socklen_t intLength = sizeof(int);
	
	int counter = 0, on = 1;
	char temp;
	char buffer[bufferSize];
	struct sockaddr_in serverAddr;
	struct sockaddr_in clientAddr;

	fd_set read_fd;
	
	struct timeval timeout;
	timeout.tv_sec = 20;
	timeout.tv_usec = 0;
 
	// Pobranie deskryptora gniazda	
	if((socketDescriptor = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		// Jeśli wystąpił błąd
		perror("[SERVER] Socket - Error");
		exit (-1);
	}
	else {
		printf("[SERVER] Socket - OK\n");
	}

	// Pozwolenie na ponowne użycie desktyptora
	if((responseValue = setsockopt(socketDescriptor, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof(on))) < 0) {
		perror("[SERVER] Socket set options - Error");
		close(socketDescriptor);
		exit (-1);
	}
	else {
		printf("[SERVER] Socket set options - OK\n");
	}

	// Bindowanie do adresu
	memset(&serverAddr, 0x00, sizeof(struct sockaddr_in));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(GUI_PORT);
	serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
        
	if((responseValue = bind(socketDescriptor, (struct sockaddr *)&serverAddr, sizeof(serverAddr))) < 0) {
		perror("[SERVER] Bind - Error");
		close(socketDescriptor);
		exit(-1);
	}
	else {
		printf("[SERVER] Bind - OK\n");
	}

	// Włączenie nasłuchiwania w serwerze
	// Ustwienie limitu połączeń
	if((responseValue = listen(socketDescriptor, CONNECTIONS_LIMIT)) < 0) {
		perror("[SERVER] Listen - Error");
		close(socketDescriptor);
		exit (-1);
	}
	else {
		printf("[SERVER] Listen - OK\n");
		printf("[SERVER] Ready for connections\n");
	}
	
	// Pobranie numeru portu
	socklen_t addr_len = sizeof(struct sockaddr_in);
	
	if (getsockname(socketDescriptor, (struct sockaddr *)&serverAddr, &addr_len) == -1) {
		perror("[SERVER] Get socket name - Error");
	}
	else {
		printf("[SERVER] Get socket name - OK\n");
		printf("[SERVER] Server: %s:%d\n", "http://localhost", ntohs(serverAddr.sin_port));
	}

	while(instance->running)
	{

		// Przyjęcie przychodzącego połączenia
		if((socketDescriptorConn = accept(socketDescriptor, (struct sockaddr *)&clientAddr, &addr_len)) < 0) {
			perror("[SERVER] Accept - Error");
			close(socketDescriptor);
			exit (-1);
		}
		else {
			#ifdef DEBUG
			printf("[SERVER] Accept - OK\n");
			#endif
		}

		// Pobranie adresu IP klienta
		#ifdef DEBUG
		printf("[SERVER] Connection from client with IP: %s\n", inet_ntoa(clientAddr.sin_addr));
		#endif

		// Włączenie funkcji select w oczekiwaniu na dane
		FD_ZERO(&read_fd);
		FD_SET(socketDescriptorConn, &read_fd);
		responseValue = select(socketDescriptorConn + 1, &read_fd, NULL, NULL, &timeout);
		
		if((responseValue == 1) && (FD_ISSET(socketDescriptorConn, &read_fd))) {
		
			// Odebranie danych od klienta
			counter = 0;

			// Odczytanie danych
			responseValue = read(socketDescriptorConn, &buffer[counter], (bufferSize - counter));

			if(responseValue < 0) {
				#ifdef DEBUG
				perror("[SERVER] Read - Error");
				#endif
				close(socketDescriptor);
				close(socketDescriptorConn);
				exit (-1);
			}
			else if (responseValue == 0) {
				#ifdef DEBUG
				printf("[SERVER] Read - Client closed\n");
				#endif
				close(socketDescriptor);
				close(socketDescriptorConn);
				exit(-1);
			}
			else {
				counter += responseValue;
				#ifdef DEBUG
				printf("[SERVER] Read - OK\n");
				#endif
			}
		}
		else if (responseValue < 0) {
			#ifdef DEBUG
			perror("[SERVER] Select - Error");
			#endif
			close(socketDescriptor);
			close(socketDescriptorConn);
			exit(-1);
		}
		else {
			#ifdef DEBUG
			printf("[SERVER] Select - Timeout\n");
			#endif
			close(socketDescriptor);
			close(socketDescriptorConn);
			exit(-1);
		}

		// Wypisanie pobranych danych
		string buffString = buffer;
		string path = getRequestPath(buffString);
		#ifdef DEBUG
		printf("[SERVER] Path: %s\n", path.c_str());
		#endif

		// Wysłanie odpowiedzi klientowi
		#ifdef DEBUG
		printf("[SERVER] Sending response to client\n");
		#endif
		
		string contentType;
		string response = instance->onRequestCome(contentType, path);
		string wholeResponse = wrapOkResponse(contentType, response);
		responseValue = write(socketDescriptorConn, wholeResponse.c_str(), wholeResponse.length());

		if(responseValue == -1) {
			#ifdef DEBUG
			perror("[SERVER] Write - Error");
			#endif
			close(socketDescriptor);
			close(socketDescriptorConn);
			exit(-1);
		}
		else {
			#ifdef DEBUG
			printf("[SERVER] Write - OK\n");
			#endif
		}

		close(socketDescriptorConn);	
	}
	
	// Zakmniecie gniazd
	close(socketDescriptor);

	return 0;
}

// zwraca elementy z kolejki jako obiekt JSON
string Module2::getQueueElements()
{
	std::vector<std::string> domains;
	int count = 0;

	char* buffer = new char[MAX_MSG_LEN];

	while(mq_receive(this->requestQueue, buffer, MAX_MSG_LEN, 0) != -1 && count < JSON_BATCH_LIMIT)
	{ 
	  QUEUED_QUERY qq = QUEUED_QUERY();
		memcpy(&qq, buffer, sizeof(QUEUED_QUERY));
		char* name_buffer = new char[qq.name_len + 1];
		name_buffer[qq.name_len] = '\0';
		memcpy(name_buffer, buffer + sizeof(QUEUED_QUERY), qq.name_len);
		string odebr = string((const char*) name_buffer);
		delete[] name_buffer;
		domains.push_back(odebr); // dodajemy domenę do wektora
	}

	delete[] buffer;

	if(domains.size() == 0)
	{
		return "[]";
	} else 
	{
		ostringstream imploded;
		copy(domains.begin(), domains.end(), std::ostream_iterator<std::string>(imploded, "\",\""));
		string json = "[\"" + imploded.str() + "\"]";
		return json;
	}
}

string Module2::saveToConfig(string &path)
{
	vector<string> parts = explode(path,':'); // ściezka typu config:domain:action
	if(parts.size() < 3)
	{
		return "Wrong number of parameters in url (should be &gt;= 3)";
	}

	string domain = parts.at(1);
	string action = parts.at(2);

	if(action == "allow")
	{
		this->xmlHandler->addWhiteListRule(domain);
	}
	else if(action == "block") 
	{
		this->xmlHandler->addBlackListRule(domain);
	}
	else if(action == "redirect")
	{
		if(parts.size() < 4)
		{
			return "Wrong number of parameters in url (should be &gt;= 4)";
		}
		this->xmlHandler->addRedirectListRule(domain, parts.at(3));
	}
	else if(action == "delete")
	{
		this->xmlHandler->deleteRule(domain);
	}

	return "success";
}


string Module2::onRequestCome(string &contentType, string &path)
{
	if(path == "config")
	{
		contentType = HTTP_CONTENT_TYPE_HTML;
		return getFileContents("web/przyklad_config.html");	
	} 
	else if(path == ROUTE_QUEUE)
	{
		contentType = HTTP_CONTENT_TYPE_HTML;
		return this->getQueueElements();
	} 
	else if(path.substr(0, ROUTE_CONFIG_SIZE) == ROUTE_CONFIG)
	{
		contentType = HTTP_CONTENT_TYPE_HTML;
		return this->saveToConfig(path);
	} 
	else if(path == "time")
	{
		contentType = HTTP_CONTENT_TYPE_HTML;
		struct timeval tim;  
	    gettimeofday(&tim, NULL);  
	    double t1=tim.tv_sec+(tim.tv_usec/1000000.0);  
		return to_string(t1); // dla przykładu
	} 
	else if(path == "web/style.css")
	{
		contentType = HTTP_CONTENT_TYPE_CSS;
		return getFileContents("web/style.css");	
	}
	else if(path == "web/jquery.js")
	{
		contentType = HTTP_CONTENT_TYPE_JS;
		return getFileContents("web/jquery.js");
	}
	else if(path == "web/script.js")
	{
		contentType = HTTP_CONTENT_TYPE_JS;
		return getFileContents("web/script.js");
	}
	else
	{
		contentType = HTTP_CONTENT_TYPE_HTML;
		return getFileContents("web/index.html");
	}
}

string Module2::getFileContents(string fileName)
{
	ifstream f(fileName);

	if(!f.good())
	     return "404";

	stringstream ss;
	string line;
	while(getline(f,line))
	{
		ss << line << endl;
	}

	return ss.str();
}