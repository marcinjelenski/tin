#include "msghandler.h"

int MsgHandler::initQueues()
{
	// Kolejka do odczytu/zapisu, tworzona jesli jeszcze nie istnieje.
	int flags1 = O_RDWR | O_CREAT;
	int flags2 = O_RDWR | O_CREAT;// | O_NONBLOCK;
	int mode = S_IRWXU | S_IRWXG | S_IRWXO;
	// attr dla nowej kolejki
	struct mq_attr attr;
	attr.mq_flags = 0;
	attr.mq_maxmsg = 10;
	attr.mq_msgsize = MAX_QUEUE_SIZE;
	attr.mq_curmsgs = 0;

	if (mq_unlink(REQUEST_QUEUE_NAME) == -1)
	{ 
		// perror("unlink REQUEST_QUEUE"); 
	}
	this->msg_queue_request = mq_open(REQUEST_QUEUE_NAME, flags2, mode, &attr);
	if (this->msg_queue_request == -1)
	{
		perror("init msg_queue_request");
		return -1;
	}
	// Utworz kolejke wejsciowa.
	if (mq_unlink(RESPONSE_QUEUE_NAME) == -1)
	{ 
		// perror("unlink RESPONSE_QUEUE"); 
	}
	this->msg_queue_response = mq_open(RESPONSE_QUEUE_NAME, flags1, mode, NULL);
	if (this->msg_queue_response == -1)
	{
		perror("init msg_queue_response");
		return -1;
	}
	return 0;
}

int MsgHandler::closeQueues()
{
	bool error = false;
	// Zamknij kolejki.
	if (mq_close(this->msg_queue_request) == -1)
	{
		perror("closing msg_queue_request");
		error = true;
	}
	if (mq_close(this->msg_queue_response) == -1)
	{
		perror("closing msg_queue_response");
		error = true;
	}
	if (error)
	{
		return -1;
	}
	return 0;
}

mqd_t MsgHandler::getRequestQueue(char mode)
{
	int flags = 0;
	// Tryb odczytu.
	if (mode == 'r')
	{
		flags = O_RDONLY | O_NONBLOCK;
	}
	// Tryb zapisu.
	else if (mode == 'w')
	{
		flags = O_WRONLY |  O_NONBLOCK;
	}
	// Nieprawidlowy tryb.
	else
	{
		return -2;
	}
	mqd_t msg_queue = mq_open(REQUEST_QUEUE_NAME, flags);
	if (msg_queue == -1)
	{
		perror("getRequestQueue");
		return -1;
	}
	return msg_queue;
}

mqd_t MsgHandler::getResponseQueue(char mode)
{
	int flags = 0;
	// Tryb odczytu.
	if (mode == 'r')
	{
		flags = O_RDONLY;
	}
	// Tryb zapisu.
	else if (mode == 'w')
	{
		flags = O_WRONLY;
	}
	// Nieprawidlowy tryb.
	else
	{
		return -2;
	}
	mqd_t msg_queue = mq_open(RESPONSE_QUEUE_NAME, flags);
	if (msg_queue == -1)
	{
		perror("getResponseQueue");
		return -1;
	}
	return msg_queue;
}