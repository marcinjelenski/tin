#include "rule.h"
#include <iostream>

Rule::Rule()
{
	this->rule_type = RuleType::EMPTY;
	this->name = string("");
	this->redirect_ip = string("");
}

void Rule::setRuleType(RuleType rule_type)
{
	this->rule_type = rule_type;
}

void Rule::setName(string name)
{
	this->name = string(name);
}

void Rule::setRedirectIP(string redirect_ip)
{
	this->redirect_ip = string(redirect_ip);
}

RuleType Rule::getRuleType()
{
	return this->rule_type;
}

string Rule::getName()
{
	return this->name;
}

string Rule::getRedirectIP()
{
	return this->redirect_ip;
}