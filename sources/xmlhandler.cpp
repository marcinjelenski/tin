#include "xmlhandler.h"

XmlHandler::XmlHandler()
{
	this->doc = new xml_document<>;
	this->filepath = "";
	this->black_list = nullptr;
	this->white_list = nullptr;
	this->redirect_list = nullptr;
}

XmlHandler::~XmlHandler()
{
	delete this->doc;
}

void XmlHandler::createEmptyConfig(const string path)
{
	// Utworz nowy dokument.
	xml_document<> doc;
	// Utworz wezel glowny (config).
	xml_node<>* config = doc.allocate_node(node_element, CFG_NODE_NAME);
	doc.append_node(config);
	// Utworz wezel black_list i dolacz do config.
	xml_node<>* black_list = doc.allocate_node(node_element, BL_NODE_NAME);
	config->append_node(black_list);
	// Utworz wezel white_list i dolacz do config.
	xml_node<>* white_list = doc.allocate_node(node_element, WL_NODE_NAME);
	config->append_node(white_list);
	// Utworz wezel redirect_list i dolacz do config.
	xml_node<>* redirect_list = doc.allocate_node(node_element, REDIR_NODE_NAME);
	config->append_node(redirect_list);
	// Zapisz plik.
	std::ofstream file_stored(path);
	file_stored << XML_DECLARATION << '\n' << doc;
	file_stored.close();
	doc.clear();
	this->doc = new xml_document<>;
}

int XmlHandler::open(string path)
{
	if (this->filepath != string("")) 
	{
		return -1;
	}
	// Zapisz sciezke otwartego pliku.
	this->filepath = path;
	// Nowa metoda wczytywania za:
	// http://www.cplusplus.com/forum/general/155550/
	file<> in(path.c_str());
	// Parsuj wczytany plik.
	this->doc->parse<parse_declaration_node>(in.data());
	// Wczytaj glowny znacznik.
	xml_node<> *config = this->doc->first_node("config");
	// Zapisz wskazania na konkretne elementy.
	this->black_list = config->first_node(BL_NODE_NAME);
	this->white_list = config->first_node(WL_NODE_NAME);
	this->redirect_list = config->first_node(REDIR_NODE_NAME);
	cout<<"Konfiguracja XML: "<<this->filepath<<endl;
	return 0;
}

int XmlHandler::save()
{
	std::ofstream file_stored(this->filepath);
	file_stored << XML_DECLARATION <<  '\n' << 
				*(this->doc->first_node("config"));
	file_stored.close();
	return 0;
}

int XmlHandler::addWhiteListRule(string host_name)
{
	if (this->white_list == nullptr) 
	{
		return -1;
	}
	this->deleteRule(host_name);
	xml_node<>* item = this->doc->allocate_node(node_element, ITEM_NODE_NAME);
	xml_node<>* domain = this->doc->allocate_node(node_element, DOMAIN_NODE_NAME);
	domain->value(this->doc->allocate_string(host_name.c_str()));
	item->append_node(domain);
	this->white_list->append_node(item);
	this->save();
	return 0;
}

int XmlHandler::addBlackListRule(string host_name)
{
	if (this->black_list == nullptr) 
	{
		return -1;
	}
	this->deleteRule(host_name);
	xml_node<>* item = this->doc->allocate_node(node_element, ITEM_NODE_NAME);
	xml_node<>* domain = this->doc->allocate_node(node_element, DOMAIN_NODE_NAME);
	domain->value(this->doc->allocate_string(host_name.c_str()));
	item->append_node(domain);
	this->black_list->append_node(item);
	this->save();
	return 0;
}

int XmlHandler::addRedirectListRule(string host_name, string new_ip_string)
{
	if (this->redirect_list == nullptr) 
	{
		return -1;
	}
	this->deleteRule(host_name);
	xml_node<>* item = this->doc->allocate_node(node_element, ITEM_NODE_NAME);
	xml_node<>* domain = this->doc->allocate_node(node_element, DOMAIN_NODE_NAME);
	xml_node<>* new_ip = this->doc->allocate_node(node_element, REDIRECTIP_NODE_NAME);
	domain->value(this->doc->allocate_string(host_name.c_str()));
	new_ip->value(this->doc->allocate_string(new_ip_string.c_str()));
	item->append_node(domain);
	item->append_node(new_ip);
	this->redirect_list->append_node(item);
	this->save();
	return 0;
}

bool XmlHandler::getRule(string host_name, Rule* rule)
{
	// Wyszukaj w regulach black list.
	for(xml_node<> *pNode=this->black_list->first_node(ITEM_NODE_NAME);
		pNode;
		pNode=pNode->next_sibling())
	{
		string domain = pNode->first_node(DOMAIN_NODE_NAME)->value();
		if (host_name == domain)
		{
			rule->setRuleType(RuleType::BLOCK);
			rule->setName(domain);
			return true;
		}
	}
	// Wyszukaj w regulach white list.
	for(xml_node<> *pNode=this->white_list->first_node(ITEM_NODE_NAME);
		pNode;
		pNode=pNode->next_sibling())
	{
		string domain = pNode->first_node(DOMAIN_NODE_NAME)->value();
		if (host_name == domain)
		{
			rule->setRuleType(RuleType::ALLOW);
			rule->setName(domain);
			return true;
		}
	}
	// Wyszukaj w regulach redirect list.
	for(xml_node<> *pNode=this->redirect_list->first_node(ITEM_NODE_NAME);
		pNode;
		pNode=pNode->next_sibling())
	{
		string domain = pNode->first_node(DOMAIN_NODE_NAME)->value();
		string new_ip = pNode->first_node(REDIRECTIP_NODE_NAME)->value();
		if (host_name == domain)
		{
			rule->setRuleType(RuleType::REDIRECT);
			rule->setName(domain);
			rule->setRedirectIP(new_ip);
			return true;
		}
	}
	return false;
}

int XmlHandler::deleteRule(string host_name)
{
	int deleted_rules_count = 0;
	// Wyszukaj w regulach black list.
	for(xml_node<> *pNode=this->black_list->first_node(ITEM_NODE_NAME);
		pNode;)
	{
		string domain = pNode->first_node(DOMAIN_NODE_NAME)->value();
		if (host_name == domain)
		{
			xml_node<>* nextNode = pNode->next_sibling();
			this->black_list->remove_node(pNode);
			deleted_rules_count += 1;
			pNode = nextNode;
		}
		else
		{
			pNode=pNode->next_sibling();
		}
	}
	// Wyszukaj w regulach white list.
	for(xml_node<> *pNode=this->white_list->first_node(ITEM_NODE_NAME);
		pNode;)
	{
		string domain = pNode->first_node(DOMAIN_NODE_NAME)->value();
		if (host_name == domain)
		{
			xml_node<>* nextNode = pNode->next_sibling();
			this->white_list->remove_node(pNode);
			deleted_rules_count += 1;
			pNode = nextNode;
		}
		else
		{
			pNode=pNode->next_sibling();
		}
	}
	// Wyszukaj w regulach redirect list.
	for(xml_node<> *pNode=this->redirect_list->first_node(ITEM_NODE_NAME);
		pNode;)
	{
		string domain = pNode->first_node(DOMAIN_NODE_NAME)->value();
		if (host_name == domain)
		{
			xml_node<>* nextNode = pNode->next_sibling();
			this->redirect_list->remove_node(pNode);
			deleted_rules_count += 1;
			pNode = nextNode;
		}
		else
		{
			pNode=pNode->next_sibling();
		}
	}
	if(deleted_rules_count > 0)
	{
		this->save();
	}
	return deleted_rules_count;
}

int XmlHandler::getRuleCount()
{
	int rule_counter = 0;
	// Iteruj po black liscie i zlicz reguly.
	for(xml_node<> *pNode=this->black_list->first_node(ITEM_NODE_NAME);
			pNode;
			pNode=pNode->next_sibling())
	{
		rule_counter += 1;
	}
	// Iteruj po white liscie i zlicz reguly.
	for(xml_node<> *pNode=this->white_list->first_node(ITEM_NODE_NAME);
			pNode;
			pNode=pNode->next_sibling())
	{
		rule_counter += 1;
	}
	// Iteruj po redirect liscie i zlicz reguly.
	for(xml_node<> *pNode=this->redirect_list->first_node(ITEM_NODE_NAME);
			pNode;
			pNode=pNode->next_sibling())
	{
		rule_counter += 1;
	}
	return rule_counter;
}

int XmlHandler::getAllRules(Rule* rule_set, int n_rules)
{
	int current_index = 0;
	// Wyszukaj w regulach black list.
	for(xml_node<> *pNode=this->black_list->first_node(ITEM_NODE_NAME);
		pNode;
		pNode=pNode->next_sibling())
	{
		string domain = pNode->first_node(DOMAIN_NODE_NAME)->value();
		rule_set[current_index].setRuleType(RuleType::BLOCK);
		rule_set[current_index].setName(domain);
		current_index += 1;
		if (current_index == n_rules)
		{
			return current_index;
		}
	}
	// Wyszukaj w regulach white list.
	for(xml_node<> *pNode=this->white_list->first_node(ITEM_NODE_NAME);
		pNode;
		pNode=pNode->next_sibling())
	{
		string domain = pNode->first_node(DOMAIN_NODE_NAME)->value();
		rule_set[current_index].setRuleType(RuleType::ALLOW);
		rule_set[current_index].setName(domain);
		current_index += 1;
		if (current_index == n_rules)
		{
			return current_index;
		}
	}
	// Wyszukaj w regulach redirect list.
	for(xml_node<> *pNode=this->redirect_list->first_node(ITEM_NODE_NAME);
		pNode;
		pNode=pNode->next_sibling())
	{
		string domain = pNode->first_node(DOMAIN_NODE_NAME)->value();
		string new_ip = pNode->first_node(REDIRECTIP_NODE_NAME)->value();
		rule_set[current_index].setRuleType(RuleType::REDIRECT);
		rule_set[current_index].setName(domain);
		rule_set[current_index].setRedirectIP(new_ip);
		current_index += 1;
		if (current_index == n_rules)
		{
			return current_index;
		}
	}
	return current_index;
}

