/////////////////
// konfiguracja
/////////////////
var DELAY = 300;
var DELAY_ERR = 2000;
/////////////////

var pendingHosts = [];

function onButtonClick(btn, btnUrl, destParent)
{
    $.ajax({
        url: btnUrl,
        type: 'GET',
        data: {},
    })
    .done(function (data) {
        console.log(data);
        var par = btn.parent().parent();
        var clone = par.clone();
        clone.addClass('small');

        destParent.prepend(clone);
        clone.hide();
        clone.show(200);

        par.hide(200, function () {
            btn.destroy(200);
        });
        initClicks();
    })
    .fail(function () {
        console.log("error");
    })
    .always(function () {
        console.log("complete");
    });
}

function initClicks()
{    
    $(".row a").unbind("click").on('click', function (e) {
        e.preventDefault();
        var btn = $(this);
        if(btn.hasClass('allow'))
        {
            onButtonClick(btn, btn.attr("href"), $("#allowed_content"));
        }
        else if(btn.hasClass('block'))
        {
            onButtonClick(btn, btn.attr("href"), $("#blocked_content"));
        }
        else if(btn.hasClass('redirect'))
        {
            var destIp = window.prompt("Adres IP przekierowania","127.0.0.1");
            if(destIp.trim())
            {
                onButtonClick(btn, btn.attr("href") + ":" + destIp.trim(), $("#config_content"));
            }
        }
    });
}

function getQueueItem(name)
{
    var inner = $("<div>").addClass("buttons");
    
    var btnBlock = $("<a>").attr("href", "/config:"+name+":block")
            .addClass("btn").addClass("right").addClass("block")
            .html("zablokuj");
    
    var btnAllow = $("<a>").attr("href", "/config:" + name + ":allow")
            .addClass("btn").addClass("right").addClass("allow")
            .html("zezwól");   

    var btnRedirect = $("<a>").attr("href", "/config:" + name + ":redirect")
            .addClass("btn").addClass("right").addClass("redirect")
            .html("redirect");
    
    inner.append(btnBlock).append(btnAllow).append(btnRedirect);
    
    var result = $("<div>").addClass("row").append(inner).append(name);
    return result;
}

function arrayObjectIndexOf(myArray, searchTerm) {
    for (var i = 0, len = myArray.length; i < len; i++) {
        if (myArray[i] === searchTerm)
        {
            return i;
        }
    }
    return -1;
}

function loadQueue()
{
    $.ajax({
        dataType: "json",
        url: 'queue',
        type: 'get',
        data: {},
        success: function (data) {
            loadTime();
            // dla wszystkich nazw z tablicy JSON
            $.each(data, function (i, domainName)
            {
                if (arrayObjectIndexOf(pendingHosts, domainName) >= 0 || !domainName.trim())
                {
                    // nie dodajemy duplikatów i pustych
                } else
                {
                    pendingHosts.push(domainName);
                    $("#queue_content").append(getQueueItem(domainName));
                    initClicks();
                }
            });
            setTimeout(function () {
                loadQueue();
            }, DELAY);
        },
        error: function () {
            setTimeout(function () {
                loadQueue();
            }, DELAY_ERR);
        }
    });
}

function loadTime()
{
    if($("#queue_time").html() == '.')
    {
        $("#queue_time").html('..');
    }
    else if($("#queue_time").html() == '..')
    {
        $("#queue_time").html('...');
    }
    else
    {
        $("#queue_time").html('.');
    }
}

$(document).ready(function ()
{
    loadQueue();
});